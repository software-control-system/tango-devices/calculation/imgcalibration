#include <algorithm>
#include <tango.h>

void usage()
{
  std::cout << "Usage : calib_save_img [attribute] [file]" << std::endl;
}

int main(int argc, char* argv[])
{
  if (argc != 3)
  {
    usage();
    return 1;
  }

  std::string attr_name = argv[1];
  std::string filename  = argv[2];

  //- add extensions
  filename = filename + ".private_format";
  int w,h;

  try
  {
    Tango::DeviceAttribute d = Tango::AttributeProxy( attr_name ).read();
    w = d.dim_x;
    h = d.dim_y;

    std::vector<unsigned short> v;
    unsigned short* v_buf = new unsigned short[w * h];
    d >> v;
    std::copy( v.begin(), v.end(), v_buf );
    

    std::ofstream file( filename.c_str(), ios::binary );
    file.write( reinterpret_cast<char*>(&w), sizeof(long) );
    file.write( reinterpret_cast<char*>(&h), sizeof(long) );
    file.write( reinterpret_cast<char*>(v_buf), w * h * sizeof(unsigned short) );

  }
  catch( std::bad_alloc& )
  {
    std::cerr << "BAD ALLOC" << std::endl;
    return 2;
  }
  catch( Tango::DevFailed& df )
  {
    std::cerr << "TANGO ERROR" << std::endl;
    std::cerr << df.errors[0].desc << std::endl;
    return 4;
  }

  std::cout << "SUCCESS !" << std::endl;
  return 0;
}

