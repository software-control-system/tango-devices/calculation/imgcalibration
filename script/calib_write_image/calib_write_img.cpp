#include <algorithm>
#include <tango.h>

void usage()
{
  std::cout << "Usage : calib_write_img [attribute] [file]" << std::endl;
}

int main(int argc, char* argv[])
{
  if (argc != 3)
  {
    usage();
    return 1;
  }

  std::string attr_name = argv[1];
  std::string filename  = argv[2];
  std::string cfg_filename = filename;

  //- add extensions
  filename = filename + ".private_format";
  int w,h;

  try
  {
    std::ifstream file( filename.c_str(), ios::binary );
    file.read( reinterpret_cast<char*>(&w), sizeof(int) );
    file.read( reinterpret_cast<char*>(&h), sizeof(int) );
    
    std::cout << "reading image file " << filename << std::endl;
    std::cout << "width  = "  << w << std::endl;
    std::cout << "height = " << h << std::endl;
    unsigned short* img_buf = new unsigned short[w * h];
    file.read( reinterpret_cast<char*>(img_buf), w * h * sizeof(unsigned short) );
    if ( file.fail() )
    {
      std::cerr << "FAILED READING FILE" << std::endl;
      return 2;
    }
    std::vector<unsigned short> v;
    std::copy( img_buf, img_buf + w * h, std::back_inserter(v) );
    Tango::AttributeProxy( attr_name ).write( Tango::DeviceAttribute(attr_name, v, w, h) );
    delete [] img_buf;
  }
  catch( std::bad_alloc& )
  {
    std::cerr << "BAD ALLOC" << std::endl;
    return 3;
  }
  catch( Tango::DevFailed& df )
  {
    std::cerr << "TANGO ERROR" << std::endl;
    std::cerr << df.errors[0].desc << std::endl;
    return 4;
  }

  std::cout << "SUCCESS !" << std::endl;
  return 0;
}

