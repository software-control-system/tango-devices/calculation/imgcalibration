// ============================================================================
//
// = CONTEXT
//    TANGO Project - ImgBeamAnalyzer DeviceServer - Data class
//
// = File
//    Data.cpp
//
// = AUTHOR
//    Julien Malik
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <ImgCalibrationTask.h>

#if !defined (YAT_INLINE_IMPL)
# include "ImgCalibrationTask.i"
#endif // __INLINE_IMPL___


namespace ImgCalibration_ns
{

const char* kRUNNING_STATUS_MSG = "The device is up and running";
const char* kSTANDBY_STATUS_MSG = "The device is in standby";

// ============================================================================
// ImgCalibrationConfig::ImgCalibrationConfig
// ============================================================================
ImgCalibrationConfig::ImgCalibrationConfig()
: x_spacing(0),
  y_spacing(0),
  comput_period(0)
{
}

// ============================================================================
// ImgCalibrationConfig::~ImgCalibrationConfig
// ============================================================================
ImgCalibrationConfig::~ImgCalibrationConfig()
{
}

// ============================================================================
// ImgCalibrationData::ImgCalibrationData
// ============================================================================
ImgCalibrationData::ImgCalibrationData()
: is_calibrated(false),
  x_mag_factor(0),
  y_mag_factor(0),
  model_error(0)
{
}

// ============================================================================
// ImgCalibrationData::~ImgCalibrationData
// ============================================================================
ImgCalibrationData::~ImgCalibrationData()
{
}



// ============================================================================
// ImgCalibrationTask::ImgCalibrationTask
// ============================================================================
ImgCalibrationTask::ImgCalibrationTask(Tango::DeviceImpl* _host_device, 
                                       const ImgCalibrationConfig&   _config,
                                       bool               _dev_proxy_allowed,
                                       const std::string& _image_dev_name,
                                       const std::string& _image_attr_name,
                                       bool               _auto_start,
                                       int                _mode)

: yat::Task(),
  Tango::LogAdapter(_host_device),
  dev_proxy_(0),
  dev_proxy_allowed_(_dev_proxy_allowed),
  image_dev_name_(_image_dev_name),
  image_attr_name_(_image_attr_name),
  mode_(_mode),
  config_(_config),
  state_(Tango::RUNNING),
  status_(kRUNNING_STATUS_MSG),
  initialized_(false)
{
  //- create a dummy Data instance
  try
  {
    this->data_ = new ImgCalibrationData;
  }
  catch(...)
  {
    THROW_DEVFAILED("OUT_OF_MEMORY",
                    "allocation of ImgCalibrationData failed",
                    "ImgCalibrationTask::ImgCalibrationTask");
  }


  if (this->mode_ == MODE_CONTINUOUS)
  {
    this->enable_periodic_msg(_auto_start);
    this->set_periodic_msg_period(_config.comput_period);
  }

  this->set_state_status(Tango::STANDBY, kSTANDBY_STATUS_MSG);
}

// ============================================================================
// ImgCalibrationTask::~ImgCalibrationTask
// ============================================================================
ImgCalibrationTask::~ImgCalibrationTask()
{
}


// ============================================================================
// ImgCalibrationTask::handle_message
// ============================================================================
void ImgCalibrationTask::handle_message (yat::Message& _msg)
throw (yat::Exception)
{
  switch (_msg.type())
	{
	  case yat::TASK_INIT:
	    {
  			DEBUG_STREAM << "ImgCalibrationTask::handle_message::thread is initializing" << std::endl;
        
        this->initialized_ = false;

        if (this->dev_proxy_allowed_ == true)
        {
          try
          {
            this->dev_proxy_ = new Tango::DeviceProxy(this->image_dev_name_);
            if (this->dev_proxy_ == 0)
              throw std::bad_alloc();
          }
          catch (const std::bad_alloc&)
          {
            THROW_YAT_ERROR("OUT_OF_MEMORY",
                            "DeviceProxy allocation failed",
                            "ImgCalibrationTask::handle_message");
          }
          catch (Tango::DevFailed& df)
          {
            yat4tango::TangoYATException ex(df);
            RETHROW_YAT_ERROR(ex,
                              "OUT_OF_MEMORY",
                              "DeviceProxy allocation failed",
                              "ImgCalibrationTask::handle_message");
          }
          catch (...)
          {
            THROW_YAT_ERROR("UNKNOWN_ERROR",
                            "DeviceProxy allocation failed",
                            "ImgCalibrationTask::handle_message");
          }
        }

        this->initialized_ = true;
      }
		  break;
		case yat::TASK_EXIT:
		  {
  			DEBUG_STREAM << "ImgCalibrationTask::handle_message::thread is quitting" << std::endl;
        SAFE_DELETE_PTR(this->dev_proxy_);
        this->initialized_ = false;
      }
			break;
		case yat::TASK_PERIODIC:
		  {
        if (this->initialized_ == false)
        {
          DEBUG_STREAM << "Not properly initialized : exiting..." << std::endl;
          return;
        }

        if (this->mode_ == MODE_ONESHOT)
        {
          DEBUG_STREAM << "Timeout messages are disabled in ONE SHOT mode" << std::endl;
          return;
        }

        this->set_state_status(Tango::RUNNING, kRUNNING_STATUS_MSG);

        //- get a new image and send a UNDISTORT message with it.
        isl::Image* image = 0;
        try
        {
          image = this->get_remote_image();
        }
        catch(const Tango::DevFailed& ex)
        {
          ERROR_STREAM << ex << std::endl;
          this->set_state_status(Tango::FAULT, ex.errors[0].desc);
          return;
        }
        catch(...)
        {
          this->set_state_status(Tango::FAULT, "Unknown error while getting remote image");
          return;
        }

        yat::Message* msg = 0;
        try
        {
          msg = yat::Message::allocate(kMSG_UNDISTORT, DEFAULT_MSG_PRIORITY, false);
          if (msg == 0)
            throw std::bad_alloc();
        }
        catch(...)
        {
          this->set_state_status(Tango::FAULT, "Allocation of a adtb::Message failed");
          SAFE_DELETE_PTR( image );
          return;
        }

        try
        {
          msg->attach_data(image);
        }
        catch (const Tango::DevFailed &ex)
        {
          ERROR_STREAM << ex << std::endl;
          this->set_state_status(Tango::FAULT, "Attaching data to a adtb::Message failed");
          SAFE_DELETE_PTR( image );
          SAFE_DELETE_PTR( msg );
          return;
        }
        catch(...)
        {
          this->set_state_status(Tango::FAULT, "Attaching data to a adtb::Message failed");
          SAFE_DELETE_PTR( image );
          SAFE_DELETE_PTR( msg );
          return;
        }

        //- post the message
        try
        {
          this->post(msg);
        }
	      catch (const Tango::DevFailed &ex)
        {
          ERROR_STREAM << ex << std::endl;
          this->set_state_status(Tango::FAULT, "The posting of a adtb::Message failed");
          SAFE_DELETE_PTR( msg ); //- will automatically delete the associated image
          return;
        }
        catch(...)
        {
          this->set_state_status(Tango::FAULT, "The posting of a adtb::Message failed");
          SAFE_DELETE_PTR( msg ); //- will automatically delete the associated image
          return;
        }
      }
		  break;
		case kMSG_UNDISTORT:
		  {
  		  DEBUG_STREAM << "ImgCalibrationTask::handle_message::handling UNDISTORT" << std::endl;

        if (this->mode_ == MODE_ONESHOT)
        {
          this->set_state_status(Tango::RUNNING, kRUNNING_STATUS_MSG);
        }

        isl::Image* image = 0;
        
        try
        {
          try
          {
            _msg.detach_data(image);
          }
          catch(Tango::DevFailed& df)
          {
            yat4tango::TangoYATException ex(df);
            RETHROW_YAT_ERROR(ex,
                              "SOFTWARE_FAILURE",
                              "Unable to dettach image from a UNDISTORT message",
                              "ImgCalibrationTask::handle_message");
          }
          catch(...)
          {
            THROW_YAT_ERROR("UNKNOWN_ERROR",
                            "Unable to dettach image from a UNDISTORT message",
                            "ImgCalibrationTask::handle_message");
          }
          
          try
          {
            this->undistort_i(*image);
          }
          catch(Tango::DevFailed& df)
          {
            yat4tango::TangoYATException ex(df);
            RETHROW_YAT_ERROR(ex,
                              "SOFTWARE_FAILURE",
                              "Error when undistorting an image",
                              "ImgCalibrationTask::handle_message");
          }
          catch(...)
          {
            THROW_YAT_ERROR("UNKNOWN_ERROR",
                            "Error when undistorting an image",
                            "ImgCalibrationTask::handle_message");
          }
          SAFE_DELETE_PTR(image);
        }
        catch(yat::Exception& ex)
        {
          SAFE_DELETE_PTR(image);
          if (this->mode_ == MODE_ONESHOT)
          {
            this->set_state_status(Tango::STANDBY, kSTANDBY_STATUS_MSG);
            throw;
          }
          else
          {
            this->set_state_status(Tango::FAULT, ex.errors[0].desc.c_str());
            return;
          }
        }
        catch(...)
        {
          SAFE_DELETE_PTR(image);
          if (this->mode_ == MODE_ONESHOT)
          {
            this->set_state_status(Tango::STANDBY, kSTANDBY_STATUS_MSG);
            THROW_YAT_ERROR("UNKNOWN_ERROR",
                            "Error when undistorting an image",
                            "ImgCalibrationTask::handle_message");
          }
          else
          {
            this->set_state_status(Tango::FAULT, "Unknown error while undistorting an image");
            return;
          }
        }
        if (this->mode_ == MODE_ONESHOT)
        {
          this->set_state_status(Tango::STANDBY, kSTANDBY_STATUS_MSG);
        }
  		}
  		break;
		case kMSG_CALIBRATE:
		  {
  		  DEBUG_STREAM << "ImgCalibrationTask::handle_message::handling CALIBRATE" << std::endl;

        if (this->mode_ == MODE_ONESHOT)
        {
          this->set_state_status(Tango::RUNNING, kRUNNING_STATUS_MSG);
        }

        isl::Image* image = 0;
        
        try
        {
          try
          {
            _msg.detach_data(image);
          }
          catch(Tango::DevFailed& df)
          {
            yat4tango::TangoYATException ex(df);
            RETHROW_YAT_ERROR(ex,
                              "SOFTWARE_FAILURE",
                              "Unable to dettach image from a CALIBRATE message",
                              "ImgCalibrationTask::handle_message");
          }
          catch(...)
          {
            THROW_YAT_ERROR("UNKNOWN_ERROR",
                            "Unable to dettach image from a CALIBRATE message",
                            "ImgCalibrationTask::handle_message");
          }
          
          try
          {
            this->calibrate_i(*image);
          }
          catch(Tango::DevFailed& df)
          {
            yat4tango::TangoYATException ex(df);
            RETHROW_YAT_ERROR(ex,
                              "SOFTWARE_FAILURE",
                              "Error when calibrating",
                              "ImgCalibrationTask::handle_message");
          }
          catch(...)
          {
            THROW_YAT_ERROR("UNKNOWN_ERROR",
                            "Error when calibrating",
                            "ImgCalibrationTask::handle_message");
          }
          SAFE_DELETE_PTR(image);
        }
        catch(yat::Exception&)
        {
          SAFE_DELETE_PTR(image);
          if (this->mode_ == MODE_ONESHOT)
          {
            this->set_state_status(Tango::STANDBY, kSTANDBY_STATUS_MSG);
          }
          //- in all device modes : throw the exception
          throw;
        }
        catch(...)
        {
          SAFE_DELETE_PTR(image);
          if (this->mode_ == MODE_ONESHOT)
          {
            this->set_state_status(Tango::STANDBY, kSTANDBY_STATUS_MSG);
          }
          //- in all device modes : throw the exception
          THROW_YAT_ERROR("UNKNOWN_ERROR",
                          "Error when calibrating an image",
                          "ImgCalibrationTask::handle_message");
        }
        if (this->mode_ == MODE_ONESHOT)
        {
          this->set_state_status(Tango::STANDBY, kSTANDBY_STATUS_MSG);
        }
  		}
  		break;
		case kMSG_START:
		  {
  		  DEBUG_STREAM << "ImgCalibrationTask::handle_message::handling START" << std::endl;
        this->enable_periodic_msg(true);
        //- the state/status will be updated when entering in the PERIODIC message handling routine
  		}
  		break;
		case kMSG_STOP:
		  {
  		  DEBUG_STREAM << "ImgCalibrationTask::handle_message::handling STOP" << std::endl;
        this->enable_periodic_msg(false);
        this->set_state_status(Tango::STANDBY, kSTANDBY_STATUS_MSG);
  		}
  		break;
  	default:
  		DEBUG_STREAM << "ImgCalibrationTask::handle_message::unhandled msg type received"<< std::endl;
  		break;
	}
}


// ============================================================================
// ImgCalibrationTask::get_remote_image
// ============================================================================
isl::Image* ImgCalibrationTask::get_remote_image()
  throw (Tango::DevFailed)
{
  isl::Image* image = 0;

  //-	Read the attribute
	//------------------------------------------
  Tango::DeviceAttribute dev_attr;
  int dim_x = 0;
  int dim_y = 0;
  
  try
  {
    dev_attr = this->dev_proxy_->read_attribute(this->image_attr_name_);
    dim_x = dev_attr.get_dim_x();
    dim_y = dev_attr.get_dim_y();

		//- just make a call to get_type() to test if the dev_attr is empty 
    //- if the attr is empty, a Tango::DevFailed will be thrown
    dev_attr.get_type();

  }
	catch (Tango::DevFailed &ex)
  {
    RETHROW_DEVFAILED(ex,
                      "SOFTWARE_FAILURE",
                      "Error while getting remote image",
                      "ImgCalibrationTask::get_remote_image");
  }
  catch(...)
  {
    THROW_DEVFAILED("UNKNOWN_ERROR",
                    "Error while getting remote image",
                    "ImgCalibrationTask::get_remote_image");
  }


  try
  {
    image = new isl::Image(dim_x, dim_y, isl::ISL_STORAGE_USHORT);
    if (image == 0)
      throw std::bad_alloc();
  }
  catch(std::bad_alloc &)
  {
    THROW_DEVFAILED("OUT_OF_MEMORY",
                    "Allocation of isl::Image failed [std::bad_alloc]",
                    "ImgCalibrationTask::get_remote_image");
  }
  catch(isl::Exception & ex)
  {
    Tango::DevFailed df = isl_to_tango_exception(ex);
    isl::ErrorHandler::reset();
    throw df;
  }
  catch(Tango::DevFailed & ex)
  {
    RETHROW_DEVFAILED(ex,
                      "SOFTWARE_FAILURE",
                      "Allocation of isl::Image failed [Tango::DevFailed]",
                      "ImgCalibrationTask::get_remote_image");
  }
  catch(...)
  {
    THROW_DEVFAILED("UNKNOWN_ERROR",
                    "Allocation of isl::Image failed [Tango::DevFailed]",
                    "ImgCalibrationTask::get_remote_image");
  }

  switch(dev_attr.get_type())
  {
  case Tango::DEV_UCHAR:
    {
      Tango::DevVarUCharArray* serialized_image = 0;
      
      try
      {
        if ((dev_attr >> serialized_image) == false)
        {
          SAFE_DELETE_PTR(image);
          THROW_DEVFAILED("OUT_OF_MEMORY",
                          "Extraction of data from Tango::Attribute failed",
                          "ImgCalibrationTask::get_remote_image");
        }
      }
      catch(std::bad_alloc &)
      {
        SAFE_DELETE_PTR(image);
        THROW_DEVFAILED("OUT_OF_MEMORY",
                        "Extraction of data from Tango::Attribute failed [std::bad_alloc]",
                        "ImgCalibrationTask::get_remote_image");
      }
      catch(Tango::DevFailed & ex)
      {
        SAFE_DELETE_PTR(image);
        RETHROW_DEVFAILED(ex,
                          "SOFTWARE_FAILURE",
                          "Extraction of data from Tango::Attribute failed [Tango::DevFailed]",
                          "ImgCalibrationTask::get_remote_image");
      }
      catch(...)
      {
        SAFE_DELETE_PTR(image);
        THROW_DEVFAILED("UNKNOWN_ERROR",
                        "Extraction of data from Tango::Attribute failed [unknown exception]",
                        "ImgCalibrationTask::get_remote_image");
      }

      isl::Image* uchar_image = 0;
      try
      {
        uchar_image = new isl::Image(dim_x, dim_y, isl::ISL_STORAGE_UCHAR);
        if (uchar_image == 0)
          throw std::bad_alloc();
      }
      catch(std::bad_alloc &)
      {
        THROW_DEVFAILED("OUT_OF_MEMORY",
                        "Allocation of isl::Image failed [std::bad_alloc]",
                        "ImgCalibrationTask::get_remote_image");
      }
      catch(isl::Exception & ex)
      {
        Tango::DevFailed df = isl_to_tango_exception(ex);
        isl::ErrorHandler::reset();
        throw df;
      }
      catch(Tango::DevFailed & ex)
      {
        RETHROW_DEVFAILED(ex,
                          "SOFTWARE_FAILURE",
                          "Allocation of isl::Image failed [Tango::DevFailed]",
                          "ImgCalibrationTask::get_remote_image");
      }
      catch(...)
      {
        THROW_DEVFAILED("UNKNOWN_ERROR",
                        "Allocation of isl::Image failed [Tango::DevFailed]",
                        "ImgCalibrationTask::get_remote_image");
      }
      
      try
      {
        uchar_image->unserialize( serialized_image->get_buffer() );
        uchar_image->convert( *image );
      }
      catch(isl::Exception & ex)
      {
        Tango::DevFailed df = isl_to_tango_exception(ex);
        isl::ErrorHandler::reset();
        RETHROW_DEVFAILED(df,
                          "SOFTWARE_FAILURE",
                          "Unable to convert the UChar image to a UShort image",
                          "ImgCalibrationTask::get_remote_image");
      }
      catch(...)
      {
        THROW_DEVFAILED("UNKNOWN_ERROR",
                        "Unable to convert the UChar image to a UShort image",
                        "ImgCalibrationTask::get_remote_image");
      }
      SAFE_DELETE_PTR(serialized_image);
      SAFE_DELETE_PTR(uchar_image);
    }
    break;
  case Tango::DEV_SHORT:
    {
      Tango::DevVarShortArray* serialized_image = 0;
      
      try
      {
        if ((dev_attr >> serialized_image) == false)
        {
          SAFE_DELETE_PTR(image);
          THROW_DEVFAILED("OUT_OF_MEMORY",
                          "Extraction of data from Tango::Attribute failed",
                          "ImgCalibrationTask::get_remote_image");
        }
      }
      catch(std::bad_alloc &)
      {
        SAFE_DELETE_PTR(image);
        THROW_DEVFAILED("OUT_OF_MEMORY",
                        "Extraction of data from Tango::Attribute failed [std::bad_alloc]",
                        "ImgCalibrationTask::get_remote_image");
      }
      catch(Tango::DevFailed & ex)
      {
        SAFE_DELETE_PTR(image);
        RETHROW_DEVFAILED(ex,
                          "SOFTWARE_FAILURE",
                          "Extraction of data from Tango::Attribute failed [Tango::DevFailed]",
                          "ImgCalibrationTask::get_remote_image");
      }
      catch(...)
      {
        SAFE_DELETE_PTR(image);
        THROW_DEVFAILED("UNKNOWN_ERROR",
                        "Extraction of data from Tango::Attribute failed [unknown exception]",
                        "ImgCalibrationTask::get_remote_image");
      }

      try
      {
        image->unserialize(serialized_image->get_buffer());
      }
      catch(isl::Exception & ex)
      {
        Tango::DevFailed df = isl_to_tango_exception(ex);
        isl::ErrorHandler::reset();
        RETHROW_DEVFAILED(df,
                          "SOFTWARE_FAILURE",
                          "Unable to unserialize image",
                          "ImgCalibrationTask::get_remote_image");
      }
      catch(...)
      {
        THROW_DEVFAILED("UNKNOWN_ERROR",
                        "Unable to unserialize image",
                        "ImgCalibrationTask::get_remote_image");
      }
      SAFE_DELETE_PTR(serialized_image);
     }
    break;
  case Tango::DEV_USHORT:
    {
      Tango::DevVarUShortArray* serialized_image = 0;
      
      try
      {
        if ((dev_attr >> serialized_image) == false)
        {
          SAFE_DELETE_PTR(image);
          THROW_DEVFAILED("OUT_OF_MEMORY",
                          "Extraction of data from Tango::Attribute failed",
                          "ImgCalibrationTask::get_remote_image");
        }
      }
      catch(std::bad_alloc &)
      {
        SAFE_DELETE_PTR(image);
        THROW_DEVFAILED("OUT_OF_MEMORY",
                        "Extraction of data from Tango::Attribute failed [std::bad_alloc]",
                        "ImgCalibrationTask::get_remote_image");
      }
      catch(Tango::DevFailed & ex)
      {
        SAFE_DELETE_PTR(image);
        RETHROW_DEVFAILED(ex,
                          "SOFTWARE_FAILURE",
                          "Extraction of data from Tango::Attribute failed [Tango::DevFailed]",
                          "ImgCalibrationTask::get_remote_image");
      }
      catch(...)
      {
        SAFE_DELETE_PTR(image);
        THROW_DEVFAILED("UNKNOWN_ERROR",
                        "Extraction of data from Tango::Attribute failed [unknown exception]",
                        "ImgCalibrationTask::get_remote_image");
      }

      try
      {
        image->unserialize(serialized_image->get_buffer());
      }
      catch(isl::Exception & ex)
      {
        Tango::DevFailed df = isl_to_tango_exception(ex);
        isl::ErrorHandler::reset();
        RETHROW_DEVFAILED(df,
                          "SOFTWARE_FAILURE",
                          "Unable to unserialize image",
                          "ImgCalibrationTask::get_remote_image");
      }
      catch(...)
      {
        THROW_DEVFAILED("UNKNOWN_ERROR",
                        "Unable to unserialize image",
                        "ImgCalibrationTask::get_remote_image");
      }
      SAFE_DELETE_PTR(serialized_image);
    }
    break;
  default:
    {
      SAFE_DELETE_PTR(image);
      THROW_DEVFAILED("SOFTWARE_FAILURE",
                      "The remote attribute must be of type DEV_UCHAR, DEV_SHORT or DEV_USHORT",
                      "ImgCalibrationTask::get_remote_image");
     }
    break;
  }

  return(image);
}


// ============================================================================
// ImgCalibrationTask::undistort_i
// ============================================================================
void ImgCalibrationTask::undistort_i(isl::Image & source_image)
  throw (Tango::DevFailed)
{
  YAT_TRACE("ImgCalibrationTask::undistort_i");

  //- Try to allocate the returned Data
  ImgCalibrationData* data = 0;
  try
  {
    //- create an instance of Data...
    data = new ImgCalibrationData ();
    if (data == 0)
      throw std::bad_alloc ();
  }
  catch(...)
  {
    THROW_DEVFAILED("OUT_OF_MEMORY",
                    "Allocation of ImgCalibrationData failed",
                    "ImgCalibrationTask::undistort_i");
  }


  ImgCalibrationConfig config;

  ImgCalibrationData* last_data = this->data_;
  int w = source_image.width();
  int h = source_image.height();

  isl::Image* corrected_image = 0;

  try
  {
    //- make a raw copy of the configuration
    {
      yat::MutexLock guard(this->config_mutex_);
      config = this->config_;
    }
    
    data->config = config;

    //- allocate output data
    data->source_image.set_dimensions( w, h );
    data->corrected_image.set_dimensions( w, h );
    corrected_image = new isl::Image(w, h, source_image.depth());

    //- copy the calibration info, if any
    data->is_calibrated = last_data->is_calibrated;
    data->x_mag_factor = last_data->x_mag_factor;
    data->y_mag_factor = last_data->y_mag_factor;
    data->model_error = last_data->model_error;
    data->source_pattern = last_data->source_pattern;
    data->corrected_pattern = last_data->corrected_pattern;
    data->error_map = last_data->error_map;
    data->delaunay_subdiv = last_data->delaunay_subdiv;

    //- apply the undistortion
    this->calib_.undistort(source_image, *corrected_image, isl::ISL_INTERP_AREA);

    //- serialize image to output
    source_image.serialize( data->source_image.base() );
    corrected_image->serialize( data->corrected_image.base() );

    SAFE_DELETE_PTR(corrected_image);
  }
  catch(isl::Exception& ex)
  {
    SAFE_RELEASE(data);
    Tango::DevFailed df = isl_to_tango_exception(ex);
    isl::ErrorHandler::reset();
    throw df;
  }
  catch(Tango::DevFailed& ex)
  {
    SAFE_RELEASE(data);
    RETHROW_DEVFAILED(ex,
                      "SOFTWARE_FAILURE",
                      "An error occured while undistorting an image",
                      "ImgCalibrationTask::undistort_i");
  }
  catch(...)
  {
    SAFE_RELEASE(data);
    THROW_DEVFAILED("UNKNOWN_ERROR",
                    "An error occured while undistorting an image",
                    "ImgCalibrationTask::undistort_i");
  }

  {
    yat::MutexLock guard(this->data_mutex_);
    this->data_ = data;
  }
  
  if (last_data)
  {
    last_data->release();
    last_data = 0;
  }
  
}

// ============================================================================
// ImgCalibrationTask::calibrate_i
// ============================================================================
void ImgCalibrationTask::calibrate_i(isl::Image & pattern_image)
  throw (Tango::DevFailed)
{
  YAT_TRACE("ImgCalibrationTask::calibrate_i");

  //- Try to allocate the returned Data
  
  ImgCalibrationData* data = 0;
  try
  {
    //- create an instance of Data...
    data = new ImgCalibrationData ();
    if (data == 0)
      throw std::bad_alloc ();
  }
  catch(...)
  {
    THROW_DEVFAILED("OUT_OF_MEMORY",
                    "Allocation of ImgCalibrationData failed",
                    "ImgCalibrationTask::calibrate_i");
  }


  ImgCalibrationConfig config;

  int w = pattern_image.width();
  int h = pattern_image.height();

  isl::Image* corrected_pattern = 0;
  isl::Image* error_map = 0;
  isl::Image* delaunay_subdiv = 0;

  try
  {
    //- make a raw copy of the configuration
    {
      yat::MutexLock guard(this->config_mutex_);
      config = this->config_;
    }
    
    data->config = config;

    //- allocate output data
    data->source_image.set_dimensions( w, h );
    data->corrected_image.set_dimensions( w, h );
    data->source_pattern.set_dimensions( w, h );
    data->corrected_pattern.set_dimensions( w, h );
    data->error_map.set_dimensions( w, h );
    data->delaunay_subdiv.set_dimensions( w, h );

    corrected_pattern = new isl::Image(w, h, pattern_image.depth());

    //- configure the calibration engine
    this->calib_.config.x_spacing = config.x_spacing;
    this->calib_.config.y_spacing = config.y_spacing;

    this->calib_.config.roi[0] = config.roi[0];
    this->calib_.config.roi[1] = config.roi[1];
    this->calib_.config.roi[2] = config.roi[2];
    this->calib_.config.roi[3] = config.roi[3];

    //- processing
    this->calib_.learn(pattern_image);

    //- undistort the pattern
    this->calib_.undistort(pattern_image, *corrected_pattern, isl::ISL_INTERP_AREA);
    
    //- copy the undistortion result into error_map
    error_map = new isl::Image(*corrected_pattern);
    delaunay_subdiv = new isl::Image(pattern_image);
    
    //- generate delaunay_subdiv & error_map
    this->calib_.draw_delaunay (*delaunay_subdiv, 1023, 1, isl::ISL_LINETYPE_4);
    this->calib_.draw_blobs    (*delaunay_subdiv, 1023, 1, isl::ISL_LINETYPE_4);
    this->calib_.draw_error_map(*error_map, 30,   1023, 1, isl::ISL_LINETYPE_4);
    
    //- serialize the images
    pattern_image    . serialize( data->source_pattern.base()    );
    corrected_pattern->serialize( data->corrected_pattern.base() );
    delaunay_subdiv  ->serialize( data->delaunay_subdiv.base()   );
    error_map        ->serialize( data->error_map.base()         );
    data->source_image    = data->source_pattern;
    data->corrected_image = data->corrected_pattern;
    
    data->is_calibrated = true;
    data->model_error   = this->calib_.model_error();
    data->x_mag_factor  = this->calib_.x_mag_factor();
    data->y_mag_factor  = this->calib_.y_mag_factor();

    SAFE_DELETE_PTR(corrected_pattern);
    SAFE_DELETE_PTR(delaunay_subdiv);
    SAFE_DELETE_PTR(error_map);
  }
  catch(isl::Exception& ex)
  {
    SAFE_RELEASE(data);
    Tango::DevFailed df = isl_to_tango_exception(ex);
    isl::ErrorHandler::reset();
    throw df;
  }
  catch(Tango::DevFailed& ex)
  {
    SAFE_RELEASE(data);
    RETHROW_DEVFAILED(ex,
                      "SOFTWARE_FAILURE",
                      "An error occured while learning a calibration pattern",
                      "ImgCalibrationTask::calibrate_i");
  }
  catch(...)
  {
    SAFE_RELEASE(data);
    THROW_DEVFAILED("UNKNOWN_ERROR",
                    "An error occured while learning a calibration pattern",
                    "ImgCalibrationTask::calibrate_i");
  }

  ImgCalibrationData* last_data = this->data_;
  
  {
    yat::MutexLock guard(this->data_mutex_);
    this->data_ = data;
  }

  if (last_data)
  {
    last_data->release();
    last_data = 0;
  } 
}


// ============================================================================
// ImgCalibrationTask::isl_to_tango_exception
// ============================================================================
Tango::DevFailed ImgCalibrationTask::isl_to_tango_exception (const isl::Exception& e)
{
  Tango::DevErrorList error_list;

  error_list.length(e.errors.size());

  for (size_t i = 0; i < e.errors.size(); i++) 
  {
    error_list[i].reason   = CORBA::string_dup(e.errors[i].reason.c_str());
    error_list[i].desc     = CORBA::string_dup(e.errors[i].description.c_str());
    error_list[i].origin   = CORBA::string_dup(e.errors[i].origin.c_str());
    error_list[i].severity = Tango::ERR;
  }

  return Tango::DevFailed(error_list);
}




}
