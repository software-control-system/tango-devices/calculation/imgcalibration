// ============================================================================
//
// = CONTEXT
//    TANGO Project - ImgBeamAnalyzer DeviceServer - Data class
//
// = File
//    Data.i
//
// = AUTHOR
//    Julien Malik
//
// ============================================================================

namespace ImgCalibration_ns
{

// ============================================================================
// ImgCalibrationData::duplicate
// ============================================================================
YAT_INLINE
ImgCalibrationData *
ImgCalibrationData::duplicate (void)
{
  return reinterpret_cast < ImgCalibrationData * >(SharedObject::duplicate ());
}

// ============================================================================
// ImgCalibrationData::release
// ============================================================================
YAT_INLINE
void
ImgCalibrationData::release (void)
{
  SharedObject::release ();
}

// ============================================================================
// ImgCalibrationTask::get_state_status
// ============================================================================
YAT_INLINE
void
ImgCalibrationTask::get_state_status (Tango::DevState& state, std::string& status)
{
  yat::MutexLock guard(this->state_status_mutex_);
  state = this->state_;
  status = this->status_;
}

// ============================================================================
// ImgCalibrationTask::set_state_status
// ============================================================================
YAT_INLINE
void
ImgCalibrationTask::set_state_status (Tango::DevState state, const char* status)
{
  yat::MutexLock guard(this->state_status_mutex_);
  this->state_ = state;
  this->status_ = status;
}


// ============================================================================
// ImgCalibrationTask::get_config
// ============================================================================
YAT_INLINE
void
ImgCalibrationTask::get_config(ImgCalibrationConfig& c)
{
  yat::MutexLock guard(this->config_mutex_);
  c = this->config_;
}

// ============================================================================
// ImgCalibrationTask::configure
// ============================================================================
YAT_INLINE
void
ImgCalibrationTask::configure(const ImgCalibrationConfig& config)
{
  yat::MutexLock guard(this->config_mutex_);
  this->config_ = config;
  this->set_periodic_msg_period(config.comput_period);
}

// ============================================================================
// ImgCalibrationTask::get_data
// ============================================================================
YAT_INLINE
void
ImgCalibrationTask::get_data (ImgCalibrationData*& data)
  throw (Tango::DevFailed)
{
  //- enter critical section
  yat::MutexLock guard(this->data_mutex_);

  if (this->mode_ == MODE_CONTINUOUS && this->state_ == Tango::STANDBY)
    data = 0;
  else
    data = this->data_ ? this->data_->duplicate() : 0;
}

} // namespace
