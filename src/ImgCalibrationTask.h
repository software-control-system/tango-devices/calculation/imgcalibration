// ============================================================================
//
// = CONTEXT
//    TANGO Project - ImgBeamAnalyzer DeviceServer - Data class
//
// = File
//    Data.h
//
// = AUTHOR
//    Julien Malik
//
// ============================================================================

#ifndef _IMG_CALIBRATION_TASK_H_
#define _IMG_CALIBRATION_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>

#include <isl/Image.h>
#include <isl/Exception.h>
#include <isl/ErrorHandler.h>
#include <isl/calib/Calib.h>

#include <yat/threading/Task.h>
#include <yat/memory/DataBuffer.h>

#ifdef WIN32
#  pragma warning( push )
#  pragma warning( disable: 4250 ) // 'x' inherits 'y' via dominance
#endif
#include <yat4tango/CommonHeader.h>
#include <yat4tango/ExceptionHelper.h>


namespace ImgCalibration_ns
{

const int MODE_ONESHOT = 0;
const int MODE_CONTINUOUS = 1;

#define kMSG_UNDISTORT (yat::FIRST_USER_MSG)
#define kMSG_CALIBRATE (yat::FIRST_USER_MSG + 1)
#define kMSG_START     (yat::FIRST_USER_MSG + 2)
#define kMSG_STOP      (yat::FIRST_USER_MSG + 3)


class ImgCalibrationConfig
{
public :
	ImgCalibrationConfig();
	~ImgCalibrationConfig();
  
  double x_spacing;
  
  double y_spacing;

  isl::Point2D<int> roi [4];

  int comput_period;
};

class ImgCalibrationData : private yat::SharedObject
{
  friend class ImgCalibrationTask;
public:

  /**
   * Duplicate (shallow copy) this shared object 
   */
  ImgCalibrationData * duplicate (void);

  /**
   * Release this shared object 
   */
  void release (void);



  ImgCalibrationConfig config;

  bool is_calibrated;

  double x_mag_factor;
  double y_mag_factor;
  double model_error;

  yat::ImageBuffer<Tango::DevUShort> source_image;
  yat::ImageBuffer<Tango::DevUShort> corrected_image;

  yat::ImageBuffer<Tango::DevUShort> source_pattern;
  yat::ImageBuffer<Tango::DevUShort> corrected_pattern;

  yat::ImageBuffer<Tango::DevUShort> error_map;
  yat::ImageBuffer<Tango::DevUShort> delaunay_subdiv;


private:
  ImgCalibrationData (void);
  ImgCalibrationData (const ImgCalibrationData &);
  virtual ~ ImgCalibrationData (void);
  ImgCalibrationData & operator= (const ImgCalibrationData &);

};



// ============================================================================
//! Data abstraction class.
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
class ImgCalibrationTask : public yat::Task, public Tango::LogAdapter
{
public:
	//- ctor ---------------------------------
	ImgCalibrationTask (Tango::DeviceImpl * _host_device, 
                      const ImgCalibrationConfig& _config,
                      bool _dev_proxy_allowed,
                      const std::string& _image_dev_name,
                      const std::string& _image_attr_name,
                      bool _auto_start,
                      int  _mode);

	//- dtor ---------------------------------
	virtual ~ImgCalibrationTask (void);

  //- returns the last available data ------
  void get_data (ImgCalibrationData*& data)
    throw (Tango::DevFailed);

  void get_config(ImgCalibrationConfig&);

  void configure(const ImgCalibrationConfig&);

  void get_state_status (Tango::DevState& state, std::string& status);
  
  //- retrieves an isl::Image from the remote device
  isl::Image* get_remote_image()
		throw (Tango::DevFailed);

protected:
	//- handle_message -----------------------
	virtual void handle_message (yat::Message& msg)
    throw (yat::Exception);

private:
  Tango::DeviceProxy*  dev_proxy_;
  bool                 dev_proxy_allowed_;
  std::string          image_dev_name_;
  std::string          image_attr_name_;
  int                  mode_;

  isl::Calib calib_;
 
  yat::Mutex config_mutex_;
  ImgCalibrationConfig config_;

  yat::Mutex data_mutex_;
  ImgCalibrationData*  data_;

  void undistort_i(isl::Image & source_image)
		throw (Tango::DevFailed);

  void calibrate_i(isl::Image & pattern_image)
		throw (Tango::DevFailed);


  //- Error handling
  static Tango::DevFailed isl_to_tango_exception (const isl::Exception& e);
  void set_state_status(Tango::DevState state, const char* status);
  void clear_error();
  void fault(const char* description);

  yat::Mutex        state_status_mutex_;
  Tango::DevState   state_;
  std::string       status_;

  bool initialized_;

};


} // namespace

#if defined (YAT_INLINE_IMPL)
# include "ImgCalibrationTask.i"
#endif // __INLINE_IMPL__

#endif
